const Tracker = require('feedtracker');
const request = require('request');
const fs = require('fs');

const CONFIG_PATH = './config.json';
const CONFIG = JSON.parse(fs.readFileSync(CONFIG_PATH));
const AUTHORIZED_AUTHOR = "Guild Wars 2 FR";
const DEBUG = false;

const fetcher = new Tracker.Fetcher(CONFIG.interval); // Time in ms to check for new articles

fetcher.addFeed(CONFIG.rss)
  .then(() => {
    if(DEBUG){
      console.log('Success adding feed', CONFIG.rss);
    }
  })
  .catch(err => {
    console.error('Error while adding feed', err);
    process.exit(1);
  });


function send(msg){
  console.log('SEND', msg);
  request.post(CONFIG.hook, {
    json: {
      content: msg
    }
  }, (error, res, body) => {
    if (error) {
      console.error(error);
      return
    }
    console.log(`statusCode: ${res.statusCode}`);
  });
}

fetcher.on('article', (article, feedLink) => {
  if(DEBUG){
    console.log('on article event');
  }
  const AUTHOR = article.author;
  const DESC = article.description;
  const LINK = article.link;

  let intro = "Psst, les gars, ça bouge du côté de";

  const news = `
# ${intro} **${AUTHOR}** :
> ${DESC}
*Source : <${LINK}>*

>`;
  console.log(news);
  if(AUTHOR.includes(AUTHORIZED_AUTHOR)){
    if(DEBUG){
      console.log('Article from authorized author');
    }
    send(news);
  }
});

fetcher.on('err', (err, feedLink) => {
  console.error('Error ', err, feedLink);
});
