# twitter2hook

## About
Based on [rrshub(https://docs.rsshub.app/en/)] to get tweets on a rss stream
Getting Tweets from "Guild Wars 2 FR" and send them on discord via webhooks.

## Config
```{
  "rss": "rss_url", //Flux to observe
  "hook": "hook_url", //Url of the hook
  "interval": 600000 //Interval in ms for checking rss
}
```

## Usage
```node index.js```
